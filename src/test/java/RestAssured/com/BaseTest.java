package RestAssured.com;

import org.testng.Assert;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class BaseTest  extends Assert {

protected String baseURL;
protected String basePath;

    public BaseTest(){
        FileInputStream fileInputStream;
        Properties property= new Properties();
        try {
            fileInputStream = new FileInputStream("reqres.properties");
            property.load(fileInputStream);
            this.baseURL = property.getProperty("baseURL");
            this.basePath = property.getProperty("basePath");
        } catch (IOException e) {
            System.err.println("Property file does not exist!");
        }
    }
}
