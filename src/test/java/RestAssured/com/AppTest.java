package RestAssured.com;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.*;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.path.xml.element.Node;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.List;

/**
 * @author Maria_Kuznetsova1
 */
public class AppTest extends BaseTest {

    private AppTest() {
    super();
    }

    @BeforeClass
    public void setBaseParam() {
        RestAssured.baseURI = baseURL;
        RestAssured.basePath = basePath;
    }

    @Test
    public void getListUsers() {
        Response  res = given()
                .param("page", "2")
                .when()
                .get("/users")
        .then()
        .log().all()
        .extract().response();
        int status = res.getStatusCode();
        List<String> users = res.path("data");
        Assert.assertEquals(status, 200);
        Assert.assertEquals(users.size(), 6);
    }
    @Test
    public void getSingleUser(){
        Response  res = given()
                .param("id", 2)
                .when()
                .get("/users")
                .then()
                .log().all()
                .extract().response();
        int status = res.getStatusCode();
        String firstName = res.path("data.first_name");
        Assert.assertEquals(status, 200);
        Assert.assertEquals(firstName, "Janet");
    }

    @Test
    public void getSingleUserNotFound(){
        Response  res = given()
                .param("id", 23)
                .when()
                .get("/users")
                .then()
                .log().all()
                .extract().response();
        int status = res.getStatusCode();
        Assert.assertEquals(status, 404);
    }
    @Test
    public void getListUnknown(){
        Response  res = given()
                .when()
                .get("/unknown")
                .then()
                .log().all()
                .extract().response();
        int status = res.getStatusCode();
        List<String> listUnknown = res.path("data");
        Assert.assertEquals(status, 200);
        Assert.assertTrue(listUnknown.size() > 0);
    }

    @Test
    public void postCreateUser(){
        Response  res = given()
                .body("{\"name\":\"morpheus\",\"job\":\"leader\"}")
                .when()
                .contentType(ContentType.JSON)
                .post("/users")
                .then()
                .log().all()
                .extract().response();
        int status = res.getStatusCode();
        String name = res.path("name");
        Assert.assertEquals(status, 201);
        Assert.assertEquals(name, "morpheus");
    }

    @Test
    public void putUpdateUser(){
        Response  res = given()
                .param("id", 2)
                .body("{\"name\":\"morpheus\",\"job\":\"zion resident\"}")
                .when()
                .contentType(ContentType.JSON)
                .put("/users")
                .then()
                .log().all()
                .extract().response();
        int status = res.getStatusCode();
        String name = res.path("job");
        Assert.assertEquals(status, 200);
        Assert.assertEquals(name, "zion resident");
    }

    @Test
    public void deleteUser(){
        Response  res = given()
                .param("id", 2)
                .when()
                .delete("/users")
                .then()
                .log().all()
                .extract().response();
        int status = res.getStatusCode();
        Assert.assertEquals(status, 204);
    }
}
